package molitics.com.sk.molitics.network;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.util.Map;

/**
 * Created by sec on 18/05/16.
 */
public class GsonRequest<T> extends Request<T> {

    private final Gson gson = new Gson();
    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Map<String, String> parameter;
    private final Response.Listener<T> listener;
    private final int methodType;
    private final  String url;
    private final Response.ErrorListener errorListener;
    private Context context;

    public GsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers,
                       Response.Listener<T> listener, Response.ErrorListener errorListener, Map<String, String> parameter, Context context) {
        super(method, url,errorListener);

        this.headers = headers;
        this.clazz = clazz;
        this.parameter = parameter;
        this.listener = listener;
        this.methodType = method;
        this.url = url;
        this.errorListener = errorListener;
    }


    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }


    @Override
    protected void deliverResponse(T response) {
        if (response!=null)
            listener.onResponse(response);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameter != null ? parameter : super.getParams();
    }


    public Map<String, String> getParameter() {
        return parameter;
    }
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = "";
                json = new String(
                        response.data,
                        HttpHeaderParser.parseCharset(response.headers));

            longInfo(json.toString());



            return Response.success(
                    gson.fromJson(json.toString(), clazz),
                    HttpHeaderParser.parseCacheHeaders(response));

        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }

    }
    public static void longInfo(String str) {
        if (str.length() > 4000) {
            Log.d("response", str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.d("response", str);
    }

}
