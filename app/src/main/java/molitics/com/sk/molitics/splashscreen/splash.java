package molitics.com.sk.molitics.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import molitics.com.sk.molitics.R;
import molitics.com.sk.molitics.activity.HomePage;
import molitics.com.sk.molitics.customui.activity.ToolBarActivity;

/**
 * Created by sec on 18/05/16.
 */
public class splash extends ToolBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(splash.this, HomePage.class);
                startActivity(i);
                finish();

            }
        },500);




    }
}
