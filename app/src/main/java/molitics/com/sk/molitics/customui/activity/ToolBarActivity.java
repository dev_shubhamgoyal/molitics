package molitics.com.sk.molitics.customui.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import molitics.com.sk.molitics.R;
import molitics.com.sk.molitics.Utils.Utils;

/**
 * Created by sec on 18/05/16.
 */
public class ToolBarActivity extends AppCompatActivity {

    protected Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        try {
            ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void changeStatusBarColor(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor(getString(R.string.status_bar_color)));
        }
    }

    @Override
    public void setContentView(int resId)
    {
        super.setContentView(resId);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            Drawable d = Utils.getDrawableAllApi(R.drawable.ic_back_white,this);
            getSupportActionBar().setHomeAsUpIndicator(d);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public Toolbar getToolbar()
    {
        if(toolbar!=null){return toolbar;}
        return null;

    }

    @TargetApi(19)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }
}
