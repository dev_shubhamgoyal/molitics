package molitics.com.sk.molitics.activity;

import android.os.Bundle;

import molitics.com.sk.molitics.R;
import molitics.com.sk.molitics.customui.activity.ToolBarActivity;

/**
 * Created by sec on 23/05/16.
 */
public class HomePage extends ToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        setTitle("Homepage");
    }
}
