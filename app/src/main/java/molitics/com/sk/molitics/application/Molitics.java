package molitics.com.sk.molitics.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import molitics.com.sk.molitics.R;

/**
 * Created by sec on 18/05/16.
 */
public class Molitics extends Application {
    public static final String TAG = Molitics.class
            .getSimpleName();
    private RequestQueue networkRequestQueue;
    private static  Molitics mApplicationInstance;
    private static DisplayImageOptions roundedImageOptions, bigImageOptions, profileImageOptions, amentiesImageOptions;

    @Override
    public void onCreate() {
        super.onCreate();


    }
    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        Molitics.context = context;
    }

    static Context context;


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (networkRequestQueue != null) {
            networkRequestQueue.cancelAll(tag);
        }
    }

    public static synchronized Molitics getInstance() {
        return mApplicationInstance;
    }

    public RequestQueue getRequestQueue() {
        if (networkRequestQueue == null) {
            networkRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return networkRequestQueue;
    }

    public static DisplayImageOptions getRoundedImageOptions() {
        if (roundedImageOptions == null)
            initImageLoader();
        return roundedImageOptions;
    }

    public static DisplayImageOptions getBigImageOptions() {
        if (bigImageOptions == null)
            initImageLoader();
        return bigImageOptions;
    }

    public static DisplayImageOptions getProfileImageOptions() {
        if (profileImageOptions == null)
            initImageLoader();
        return profileImageOptions;
    }

    public static DisplayImageOptions getAmentiesImageOptions() {
        if (amentiesImageOptions == null)
            initImageLoader();
        return amentiesImageOptions;
    }


    private static void initImageLoader() {
        bigImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.property_blank_image)
                .showImageForEmptyUri(R.drawable.property_blank_image)
                .showImageOnFail(R.drawable.property_blank_image)
                .cacheInMemory(true)
                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new SimpleBitmapDisplayer())
                //.displayer(new FadeInBitmapDisplayer(1500))
                .build();

        roundedImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.property_blank_image)
                .showImageForEmptyUri(R.drawable.property_blank_image)
//				.showImageOnFail(R.drawable.chat_profile_icon)
                .cacheInMemory(true)
                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new RoundedBitmapDisplayer(200))
                .build();


        profileImageOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.property_blank_image)
                .showImageForEmptyUri(R.drawable.property_blank_image)
                .showImageOnFail(R.drawable.property_blank_image)
                .cacheInMemory(true)
                .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new SimpleBitmapDisplayer())
                //.displayer(new FadeInBitmapDisplayer(1500))
                .build();



        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .denyCacheImageMultipleSizesInMemory().defaultDisplayImageOptions(bigImageOptions)
                .writeDebugLogs().build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

}
